#!/usr/bin/env python

"""
Script performs the analysis of the spiral structure:
1) it calls 'connect_with_spirals' script to create a regions file with tangents to spirals
2) it calls 'cut_spirals' script to make spiral cuts and fit them
"""

import argparse
from pathlib import Path
from os import remove
import subprocess
import shutil
from libs.make_1d_psf import make_1d_psf
from libs.get_noise import get_noise
from libs.connect_with_spirals import connect_with_spirals
from libs.make_cuts import make_cuts
from libs.cut_tails import cut_tails
from libs.smooth_spirals import smooth_spirals
from libs.analyse_results import analyse_results


def rm(file_name):
    if Path(file_name).exists():
        remove(file_name)


def mv(src, dst):
    if Path(src).exists():
        shutil.move(src, dst)
    else:
        print("%s does not exist" % src)


def run(args):
    # Find noise parameters
    back_rms = get_noise(args.image, args.backpointing)
    # Compute pc per pixel scale
    pc_per_pix = args.imgscale * args.galscale
    # Create a psf slice
    psf_fwhm = make_1d_psf(args.psf, "psf_1d.dat", 1)
    print(f"psf fwhm: {psf_fwhm}")

    # 1) Make tangents
    connect_with_spirals(args.armpointing, args.xcen, args.ycen, "arm_centers.reg", "arm_tangents.reg")

    # 2) Make cuts
    make_cuts(args.image, "arm_tangents.reg", args.xcen, args.ycen, args.outdir,
              "psf_1d.dat", psf_fwhm, args.mask, pc_per_pix)

    # 2b) Cut weak tails of spirals
    cut_tails(args.outdir, back_rms, 1)

    # 3) Smooth spirals
    smooth_spirals(args.image, args.outdir, args.xcen, args.ycen, pc_per_pix)

    # 3b) Make spiral mask Do we really need this?
    # call_string = "./make_spirals_mask.py --data %s " % (args.outdir / args.passband)
    # call_string += "--xcen %1.3f --ycen %1.3f " % (deproj_x_cen, deproj_y_cen)
    # call_string += "--ell 0.0 --posang 0.0"
    # call_string += " --galname %s" % args.passband
    # print(call_string)
    # subprocess.call(call_string, shell=True)

    # 4) Create model arms Do we really need this
    # call_string = "./model_arms.py --data %s " % (args.outdir / args.passband)
    # call_string += "--xcen %1.3f --ycen %1.3f " % (deproj_x_cen, deproj_y_cen)
    # subprocess.call(call_string, shell=True)

    # 5) Perform some analysis of the results
    analyse_results(args.outdir, args.xcen, args.ycen, pc_per_pix)

    # Cleanup
    mv("arm_tangents.reg", args.outdir / "arm_tangents.reg")
    mv("arm_centers.reg", args.outdir / "{arm_centers.reg")
    mv("diff_deprojected.fits", args.outdir / "diff_deprojected.fits")
    mv("psf_1d.dat", args.outdir / "psf_1d.dat")
    rm("mask_orig.fits")
    return True


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--image", type=Path, help="File with deprojected image of the galaxy")
    parser.add_argument("--xcen", type=float, help="X coordinate of the image center")
    parser.add_argument("--ycen", type=float, help="Y coordinate of the image center")
    parser.add_argument("--galscale", type=float,
                        help="Galaxy scale in parsecs per arcsecond (depends on the galaxy distance)")
    parser.add_argument("--imgscale", type=float,
                        help="Image scale in arcseconds per pixel (0.396 for SDSS)")
    parser.add_argument("--armpointing", type=Path,
                        help="DS9 region file with a arm pointing")
    parser.add_argument("--backpointing", type=Path,
                        help="DS9 region with with a background pointing")
    parser.add_argument("--psf", type=Path, help="Path to PSF image file")
    parser.add_argument("--mask", type=Path, help="Path to mask image file", default=None)
    parser.add_argument("--outdir", type=Path, help="Results will be saved in this directory")

    args = parser.parse_args()

    res = run(args)
