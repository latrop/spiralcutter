#! /usr/bin/env python

import math
from os import path


class DS9Region(object):
    def __init__(self, region_type, params, comment="", properties=None):
        self.region_type = region_type
        self.params = params
        self.comment = comment
        if properties is not None:
            self.properties = properties
        else:
            self.properties = {}

    @classmethod
    def from_line(cls, line, default_properties=None):
        # Parameters for supported region types
        all_region_params = {"circle": ["x", "y", "radius"],
                             "ellipse": ["x", "y", "radius", "radius", "angle"],
                             "box": ["x", "y", "width", "height", "angle"],
                             "point": ["x", "y"],
                             "line": ["x1", "y1", "x2", "y2"]}
        if default_properties is None:
            default_properties = {"color": "green",
                                  "width": "1",
                                  "select": "1",
                                  "highlite": "1",
                                  "dash": "0",
                                  "fixed": "0",
                                  "edit": "1",
                                  "move": "1",
                                  "delete": "1",
                                  "include": "1",
                                  "source": "1"}
        for region_type in all_region_params.keys():
            if line.startswith("%s(" % region_type):
                # It's really a region line
                break
        else:
            # It's not a valid region line (or this region is not supported)
            return None
        # Parameters are listed between brackets
        params_beginning = line.find("(")
        params_end = line.find(")")

        # Parse values of parameters
        parameters_values = {}
        for idx, parameter_name in enumerate(all_region_params[region_type]):
            parameters_values[parameter_name] = float(line[params_beginning+1: params_end].split(",")[idx])

        # Find a comment string if any
        comment = line[line.find("#"): -1]

        # Try to parse comment line to get properties
        properties_values = {}
        for property_name in default_properties.keys():
            if property_name in comment:
                # Unpack the property value from the comment line
                property_value = line[line.find(property_name)+len(property_name)+1:].split()[0]
                properties_values[property_name] = property_value
            else:
                # Use default value
                properties_values[property_name] = default_properties[property_name]
        # Unpack unknown properties values from the line
        for substring in comment.split():
            if "=" in substring:
                property_name, property_value = substring.split("=")
                properties_values[property_name] = property_value
            else:
                properties_values[property_name] += " %s" % substring

        return cls(region_type, parameters_values, comment, properties_values)

    def to_line(self):
        line = "%s(" % self.region_type
        line += ",".join("%r" % _ for _ in self.params.values())
        line += ")"
        line += " %s" % self.comment
        return line

    def __repr__(self):
        return self.to_line()

    def __str__(self):
        return self.to_line()

    def size(self):
        """
        The method returns the value that shows the characteristic size of a
        region (area for 2D regions, length for 1D) such that the regions could
        be compared.
        """
        if self.region_type == "circle":
            return math.pi * self.params["radius"] ** 2
        if self.region_type == "ellipse":
            return math.pi * self.params["radius1"] * self.params["radius2"]
        if self.region_type == "box":
            return self.params["width"] * self.params["height"]
        if self.region_type == "point":
            return 0
        if self.region_type == "line":
            return math.hypot(self.params["x1"] - self.params["x2"],
                              self.params["y1"] - self.params["y2"])

    def center(self):
        """
        Returns the center of the region if it is possible for the region type
        """
        if self.region_type in ["circle", "ellipse", "box", "point"]:
            return self.params["x"], self.params["y"]
        if self.region_type == "line":
            xlength = abs(self.params["x1"] - self.params["x2"])
            ylength = abs(self.params["y1"] - self.params["y2"])
            xcen = self.params["x1"] + xlength / 2
            ycen = self.params["y1"] + ylength / 2
            return xcen, ycen

    def set_new_center(self, x_new, y_new):
        if self.region_type in ["circle", "ellipse", "box", "point"]:
            self.params["x"] = x_new
            self.params["y"] = y_new
        else:
            raise NotImplementedError


def parse_reg_file(reg_file_name):
    if not path.exists(reg_file_name):
        print("File %s does not exist." % reg_file_name)
        return []
    regions = []
    properties = None
    for line in open(reg_file_name):
        if line.startswith("#") or (len(line) == 0) or line.startswith("image"):
            # Comment or empty line. Just skip it
            continue
        elif "global" in line:
            properties = {}
            # Default properties line
            line = line[len("global"):]
            # Unpack properties values from the line
            for substring in line.split():
                if "=" in substring:
                    property_name, property_value = substring.split("=")
                    properties[property_name] = property_value
                else:
                    properties[property_name] += " %s" % substring
        else:
            # Region line
            new_region = DS9Region.from_line(line, default_properties=properties)
            if new_region is not None:
                regions.append(new_region)
    return regions
