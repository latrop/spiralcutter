#!/usr/bin/env python

import argparse
import shelve
import subprocess
import time
import numpy as np
from scipy.optimize import differential_evolution
from matplotlib import pyplot as plt
import matplotlib.patches as patches
from astropy.io import fits
from astropy.convolution import convolve_fft as fftconvolve
from astropy.convolution import Gaussian1DKernel


def gaussian(dist, params):
    # Unpack parameters
    amplitude = params[0]
    peak_pos = params[1]
    width = params[2]
    # Compute negative_gaussian
    flux = np.ones_like(dist) * amplitude
    flux *= np.exp(-((dist - peak_pos)**2) / (width**2))
    return flux


def gaussian_lopsided(dist, params):
    # Unpack params
    amplitude = params[0]
    peak_pos = params[1]
    width_left = params[2]
    width_right = params[3]
    # Compute exponential rise and decay
    flux = np.zeros_like(dist) + amplitude
    left_idx = dist <= peak_pos
    right_idx = dist > peak_pos
    flux[left_idx] *= np.exp(-((dist[left_idx] - peak_pos)**2) / (width_left**2))
    flux[right_idx] *= np.exp(-((dist[right_idx] - peak_pos)**2) / (width_right**2))
    return flux


class FitBuilder(object):
    def __init__(self, ax, fig, psf):
        self.ax = ax
        self.fig = fig
        # Unpack information for fitting
        db = shelve.open("to_fit.db")
        self.dist_pc = db['dist']
        self.flux = db['flux']
        self.flux_std = db['flux_std']
        self.cut_region = db['cut_region']
        self.fits_file = db['fits_file']
        db.close()
        self.discard = False
        # Parse cut region
        cut_coords = self.cut_region[self.cut_region.index("(")+1: self.cut_region.index(")")].split(",")
        self.cut_x1 = float(cut_coords[0])
        self.cut_y1 = float(cut_coords[1])
        self.cut_x2 = float(cut_coords[2])
        self.cut_y2 = float(cut_coords[3])
        # Determine which end of the cut region is closer to the image center
        y_size, x_size = fits.getdata(self.fits_file).shape
        y_cen = y_size / 2
        x_cen = x_size / 2
        dist1 = np.hypot(self.cut_x1-x_cen, self.cut_y1-y_cen)
        dist2 = np.hypot(self.cut_x2-x_cen, self.cut_y2-y_cen)
        if dist2 < dist1:
            # Swap if the order is wrong
            self.cut_x1, self.cut_x2 = self.cut_x2, self.cut_x1
            self.cut_y1, self.cut_y2 = self.cut_y2, self.cut_y1
        self.psf = psf
        self.cid = fig.canvas.mpl_connect('button_press_event', self.mouse_event)
        self.kid = fig.canvas.mpl_connect('key_press_event', self.keyboard_event)
        self.positive_gaussians_locations = []
        self.negative_gaussians_locations = []
        self.plot_instance = []
        self.start_point = None
        self.range_min = None
        self.range_max = None
        # Run ds9
        subprocess.call(f"ds9 {self.fits_file} -scale log -scale mode 99.5 &", shell=True)
        self.fit()

    def get_coords_of_point_on_cut(self, dist):
        dx = abs(self.cut_x1 - self.cut_x2)
        dy = abs(self.cut_y1 - self.cut_y2)
        cut_length = np.hypot(dx, dy)
        if (self.cut_x2 > self.cut_x1) and (self.cut_y2 > self.cut_y1):
            x = self.cut_x1 + dx * dist / cut_length
            y = self.cut_y1 + dy * dist / cut_length
        elif (self.cut_x2 > self.cut_x1) and (self.cut_y2 < self.cut_y1):
            x = self.cut_x1 + dx * dist / cut_length
            y = self.cut_y1 - dy * dist / cut_length
        elif (self.cut_x2 < self.cut_x1) and (self.cut_y2 < self.cut_y1):
            x = self.cut_x1 - dx * dist / cut_length
            y = self.cut_y1 - dy * dist / cut_length
        elif (self.cut_x2 < self.cut_x1) and (self.cut_y2 > self.cut_y1):
            x = self.cut_x1 - dx * dist / cut_length
            y = self.cut_y1 + dy * dist / cut_length
        return x, y

    def show_info_on_ds9(self):
        """
        Show information about slice location on ds9 image
        """
        # Check if ds9 is running
        while 1:
            child = subprocess.Popen(["xpaget", "ds9", "about"])
            child.communicate()
            if child.returncode == 0:
                break
            time.sleep(0.25)
        # Clean all ds9 regions
        subprocess.call("xpaset -p ds9 regions delete all", shell=True)

        # Show cut line on ds9 image
        subprocess.call(f'echo "image; {self.cut_region}" | xpaset ds9 regions', shell=True)

        # Show fit ranges
        if self.range_min is not None:
            x, y = self.get_coords_of_point_on_cut(self.range_min)
            subprocess.call(f'echo "image; point({x},{y}) # point=x" | xpaset ds9 regions', shell=True)
        if self.range_max is not None:
            x, y = self.get_coords_of_point_on_cut(self.range_max)
            subprocess.call(f'echo "image; point({x},{y}) # point=x" | xpaset ds9 regions', shell=True)

    def keyboard_event(self, event):
        if event.key == " ":
            self.discard = not self.discard
            self.fig.canvas.close()
            plt.close()

    def mouse_event(self, event):
        if self.range_min is None:
            self.range_min = event.xdata
            self.fit()
            return
        if self.range_max is None:
            if event.xdata < self.range_min:
                # Do not allow max range to be lower that min range
                return
            self.range_max = event.xdata
            self.fit()
            return

        if event.button == 2:
            # Middle mouse button: reset all added gaussians
            self.start_point = None
            self.positive_gaussians_locations = []
            self.negative_gaussians_locations = []
            self.range_min = None
            self.range_max = None
            self.fit()
            return

        if self.start_point is None:
            self.start_point = event.xdata
            try:
                self.plot_instance.append(self.ax.axvline(event.xdata))
            except TypeError:
                return
            self.fig.canvas.draw()
            return
        if event.button == 1:
            # Left mouse button: add positive gaussian
            self.positive_gaussians_locations.append([self.start_point, event.xdata])
        elif event.button == 3:
            # Right mouse button: add negative gaussian
            self.negative_gaussians_locations.append([self.start_point, event.xdata])
        self.start_point = None
        self.fit()

    def fit(self):
        """Fit cut with a set of gaussians"""
        self.show_info_on_ds9()
        if self.range_min is None:
            idx_start = 0
        else:
            idx_start = np.abs(self.dist_pc - self.range_min).argmin()
        if self.range_max is None:
            idx_end = len(self.dist_pc)
        else:
            idx_end = np.abs(self.dist_pc - self.range_max).argmin()

        self.dist_to_fit = self.dist_pc[idx_start: idx_end]
        self.flux_to_fit = self.flux.copy()[idx_start: idx_end]
        self.flux_std_to_fit = self.flux_std[idx_start: idx_end]

        # Detrend cut
        if len(self.dist_to_fit) > 20:
            d = 3
        else:
            d = 1
        x1 = np.mean(self.dist_to_fit[0:d])
        x2 = np.mean(self.dist_to_fit[-d:])
        y1 = np.mean(self.flux_to_fit[0:d])
        y2 = np.mean(self.flux_to_fit[-d:])
        trend_slope = (y2-y1) / (x2-x1)
        trend_intercept = y1 - trend_slope * x1
        self.flux_to_fit -= trend_slope * self.dist_to_fit + trend_intercept

        # Construct a fitting function
        def fitting_function(dist, params):
            # Take mean gaussian into account
            flux = gaussian_lopsided(dist, params[:4])
            # Take positive gaussians into account
            n_of_add_gaussians = len(self.positive_gaussians_locations) + len(self.negative_gaussians_locations)
            for i in range(n_of_add_gaussians):
                p_start = 4+i*3
                p_end = 4+i*3+3
                flux += gaussian(dist, params[p_start: p_end])
            return flux

        # Construct a function to calculate chi-squared
        def chisq(params):
            computed_flux = fitting_function(self.dist_to_fit, params)
            try:
                convolved_flux = fftconvolve(computed_flux, self.psf, boundary="fill")
            except Exception:
                return np.average((self.flux_to_fit - computed_flux)**2, weights=1/self.flux_std_to_fit) * 10

            return np.average((self.flux_to_fit - convolved_flux)**2, weights=1/self.flux_std_to_fit)

        # Make bounds for the main gaussian
        full_cut_width = self.dist_to_fit[-1] - self.dist_to_fit[0]
        amplitude = (min(self.flux_to_fit), 1.25*max(self.flux_to_fit))
        peak_pos = (self.dist_to_fit[0], self.dist_to_fit[-1])
        width = (0.5, 1.75*full_cut_width)
        bounds = [amplitude, peak_pos, width, width]
        # Make bounds for positive gaussians
        for marked_points in self.positive_gaussians_locations:
            start_point, end_point = marked_points
            peak = (start_point + end_point) / 2
            width = peak - start_point
            bounds.append((0.025*max(self.flux_to_fit), 0.5*max(self.flux_to_fit)))
            bounds.append([peak - full_cut_width / 20, peak + full_cut_width / 20])
            bounds.append([width/2, 2*width])
        # Make bounds for negative gaussians
        for marked_points in self.negative_gaussians_locations:
            start_point, end_point = marked_points
            peak = (start_point + end_point) / 2
            width = peak - start_point
            bounds.append((-0.5*max(self.flux_to_fit), -0.025*max(self.flux_to_fit)))  # Negative flux
            bounds.append([peak - full_cut_width / 20, peak + full_cut_width / 20])
            bounds.append([width/2, 2*width])

        # Start fitting
        print("Start fitting")
        self.fit_res = differential_evolution(func=chisq, bounds=bounds, polish=False)
        print("Fitting dones")
        self.chi_sq_value = chisq(self.fit_res.x)

        # Plot fit
        while self.plot_instance:
            self.plot_instance.pop().remove()
        # Plot data to be fitted
        self.flux_detrended = self.flux - (trend_slope * self.dist_pc + trend_intercept)
        self.plot_instance.append(self.ax.errorbar(self.dist_pc, self.flux_detrended, self.flux_std, color="g"))
        # Plot full fit
        if self.range_min is not None:
            self.plot_instance.append(self.ax.axvline(self.range_min, color="k"))
        if self.range_max is not None:
            self.plot_instance.append(self.ax.axvline(self.range_max, color="k"))
        self.full_flux_fit = fitting_function(self.dist_to_fit, self.fit_res.x)
        self.plot_instance.append(self.ax.plot(self.dist_to_fit, self.full_flux_fit, color="red")[0])
        # Plot main gaussian
        self.main_gaussian_fit = gaussian_lopsided(self.dist_to_fit, self.fit_res.x[:4])
        self.plot_instance.append(self.ax.plot(self.dist_to_fit, self.main_gaussian_fit,
                                               linestyle="--", color="k")[0])
        # Compute how would observed flux look if it was not influenced by attenuation (i.e. we
        # are going to correct the flux for all the negative gaussians in the fit)
        self.dust_corrected_flux = self.flux_to_fit.copy()
        n_of_add_gaussians = len(self.positive_gaussians_locations) + len(self.negative_gaussians_locations)
        for i in range(n_of_add_gaussians):
            p_start = 4+i*3
            p_end = 4+i*3+3
            amplitude, peak_pos, width = self.fit_res.x[p_start: p_end]
            if amplitude < 0:
                # It's a negative gaussian. Correct the flux for it
                self.dust_corrected_flux -= gaussian(self.dist_to_fit, self.fit_res.x[p_start: p_end])
        self.plot_instance.append(self.ax.plot(self.dist_to_fit, self.dust_corrected_flux,
                                               linestyle=":", color="k")[0])
        y_min = np.min(self.flux_to_fit) - 0.2*(np.max(self.flux_to_fit)-np.min(self.flux_to_fit))
        y_max = np.max(self.flux_to_fit) + 0.2*(np.max(self.flux_to_fit)-np.min(self.flux_to_fit))
        plt.gca().set_ylim(y_min, y_max)
        self.fig.canvas.draw()


def main(args):
    psf = Gaussian1DKernel(stddev=args.psf/2.355)
    fig, ax = plt.subplots()
    fit_builder = FitBuilder(ax, fig, psf)
    plt.show()
    db = shelve.open("interactive_fit.db")
    if fit_builder.discard is True:
        # User can mark cut as bad by seleecting the starting point, but closing the window without
        # setting end point of additional gaussian
        print("Discarding")
        db["good"] = False
        db.close()
        subprocess.call("xpaset -p ds9 exit", shell=True)
        return
    db["good"] = True
    db["params"] = fit_builder.fit_res.x
    db["chisq"] = fit_builder.chi_sq_value
    db["full_flux"] = fit_builder.full_flux_fit
    db["main_gaussian"] = fit_builder.main_gaussian_fit
    db["dist_to_fit"] = fit_builder.dist_to_fit
    db["flux_to_fit"] = fit_builder.flux_to_fit
    db["flux_std_to_fit"] = fit_builder.flux_std_to_fit
    db["flux_detrended"] = fit_builder.flux_detrended
    db["flux_dedusted"] = fit_builder.dust_corrected_flux
    db.close()
    # Close ds9 if not closed still
    subprocess.call("xpaset -p ds9 exit", shell=True)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("cutfile")
    parser.add_argument("psf", type=float)
    args = parser.parse_args()
    main(args)
