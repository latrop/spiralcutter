#!/usr/bin/env python

"""
Script pefroms some analysis of the results of cut_spirals.py script
"""
import glob
import os
from os import path
from os import remove
import math
import numpy as np
import matplotlib.pyplot as plt

from scipy import stats
from scipy import odr
from libs.math_funcs import determine_winding_direction
from libs.math_funcs import cartesian_to_polar_spiral

import matplotlib
try:
    os.environ["DISPLAY"]
except KeyError:
    matplotlib.use("Agg")


PHOTCOEFF = 2.5 / math.log(10)
path_to_orig_data = "/home/sergey/work/24_mol_a_spirals/s4g/data/"
orientations_file = "/home/sergey/work/24_mol_a_spirals/s4g/ascii_data/orientations.dat"


def linear(B, x):
    return B[0]*x + B[1]


def fit_by_line(x, y, sy=None):
    # Find initial conditions for ODR by scipy.stats.linregress
    slope0, intercept0, *_ = stats.linregress(x, y)
    # Perform ODR linear fit
    if sy is not None:
        data = odr.RealData(x=x, y=y, sy=sy)
    else:
        data = odr.Data(x, y)
    results = odr.ODR(data=data, model=odr.Model(linear), beta0=[slope0, intercept0]).run()
    if sy is None:
        for _ in range(25):
            y_sigma = np.sqrt(np.abs((y - linear(results.beta, x))))
            results = odr.ODR(data=odr.RealData(x=x, y=y, sy=y_sigma),
                              model=odr.Model(linear),
                              beta0=results.beta).run()
    return results


def expmag(B, x):
    """
    Exponential profile in magnitudes scale
    """
    mu0 = B[0]
    h = B[1]
    if h <= 0:
        return 1e10
    value = mu0 + PHOTCOEFF * (x/h)
    return value


def fit_by_expmag(r, mu):
    # Initial conditions
    mu0_0 = mu[0]
    dmu = (mu[-1] - mu[0])
    if dmu != 0:
        h0 = r[-1] * PHOTCOEFF / dmu
    else:
        h0 = -1
    if h0 < 0:
        h0 = 5000
    print("initial h: %1.3f" % h0)
    # Perfor the fit
    data = odr.Data(r, mu)
    results = odr.ODR(data=data, model=odr.Model(expmag),
                      beta0=[mu0_0, h0]).run()
    for _ in range(25):
        mu_sigma = np.sqrt(np.abs((mu - expmag(results.beta, r))))
        data = odr.RealData(x=r, y=mu, sy=mu_sigma)
        results = odr.ODR(data=data, model=odr.Model(expmag), beta0=results.beta).run()
    print("fitted h: %1.3f" % results.beta[1])
    return results


def sersic(B, x):
    """
    Sersic profile in magnitudes scale
    """
    mu0 = B[0]
    re = B[1]
    n = B[2]
    nu_n = 2*n - 1/3.0 + 4/(405*n) + 46/(25515 * n**2)
    return mu0 + PHOTCOEFF * nu_n * ((x/re)**(1/n))


def fit_by_sersic(r, mu):
    # Initial conditions
    mu0_0 = mu[0]
    re_0 = r[int(len(r)/4)]
    n_0 = 1.0
    # Perform fit
    data = odr.Data(r, mu)
    results = odr.ODR(data=data, model=odr.Model(sersic),
                      beta0=[mu0_0, re_0, n_0]).run()
    mu_sigma = np.sqrt(np.abs((mu - sersic(results.beta, r))))
    return odr.ODR(data=odr.RealData(x=r, y=mu, sy=mu_sigma),
                   model=odr.Model(sersic),
                   beta0=results.beta).run()


def rm(file_name):
    if path.exists(file_name):
        remove(file_name)


def analyse_results(data, xcen, ycen, pc_per_pix):
    files_with_arm_params = glob.glob(path.join(data, "arm_params*.dat"))
    # Let's go at first through all the spirals to determine the max width. We will use
    # it soon to set plots ranges along y-axes
    wmax = 0
    dmax_all = 0
    dmin_all = 1e10
    for arm_file in files_with_arm_params:
        dists, widths_left, widths_right, good_fit = np.genfromtxt(arm_file, unpack=True, usecols=[1, 5, 7, 9])
        widths_mean = (widths_left+widths_right)/2
        wmax = max(wmax, max(widths_left), max(widths_right), max(widths_mean))
        dmax_all = max(dmax_all, max(dists))
        dmin_all = min(dmin_all, min(dists))

    # Pefrom the linear regression of widths
    for arm_file in files_with_arm_params:
        arm_color = path.splitext(path.basename(arm_file))[0].split("_")[-1]
        # Load data
        dists, widths_left, widths_right, good_fit = np.genfromtxt(arm_file, unpack=True, usecols=[1, 5, 7, 9])
        good_inds = np.where(good_fit == 1)
        if len(good_inds[0]) < 7:
            print("Not enough points to fit widths for %s arm" % arm_color)
            continue
        dists = dists[good_inds]
        widths_left = widths_left[good_inds]
        widths_right = widths_right[good_inds]

        widths_mean = (widths_left+widths_right) / 2

        # Perform the regression
        fit_result = fit_by_line(dists, widths_left)
        slope_left, intercept_left = fit_result.beta
        slope_left_std, intercept_left_std = fit_result.sd_beta

        fit_result = fit_by_line(dists, widths_right)
        slope_right, intercept_right = fit_result.beta
        slope_right_std, intercept_right_std = fit_result.sd_beta

        fit_result = fit_by_line(dists, widths_mean)
        slope_mean, intercept_mean = fit_result.beta
        slope_mean_std, intercept_mean_std = fit_result.sd_beta

        # Find correlation coefficients
        pearson_left, _ = stats.pearsonr(dists, widths_left)
        pearson_right, _ = stats.pearsonr(dists, widths_right)
        pearson_mean, _ = stats.pearsonr(dists, widths_mean)

        # Save results to an ASCII file
        fout = open(path.join(data, "widths_fit_%s.dat" % arm_color), "w")
        fout.write("# slope_left  +/-   slope_right  +/-   slope_mean  +/-   ")
        fout.write("pearson_left  perason_right  pearson_mean\n")
        fout.write("%1.5e  " % slope_left)
        fout.write("%1.5e  " % slope_left_std)
        fout.write("%1.5e  " % slope_right)
        fout.write("%1.5e  " % slope_right_std)
        fout.write("%1.5e  " % slope_mean)
        fout.write("%1.5e  " % slope_mean_std)
        fout.write("%1.3f  " % pearson_left)
        fout.write("%1.3f  " % pearson_right)
        fout.write("%1.3f\n" % pearson_mean)

        # Plot figures
        dmin = min(dists)
        dmax = max(dists)
        plt.figure(figsize=(15, 5))
        plt.subplot(131)
        plt.plot(dists, widths_left, "ko")
        plt.plot([dmin, dmax], [dmin*slope_left+intercept_left, dmax*slope_left+intercept_left], linestyle="solid")
        plt.xlim(dmin_all, dmax_all)
        plt.ylim(0, wmax)
        plt.xlabel("Distance")
        plt.ylabel("Width inner")
        e, m = np.format_float_scientific(slope_left).split("e")
        to_show = r"\begin{eqnarray*} "
        to_show += r"&\kappa&=%1.3f\\" % pearson_left
        to_show += r"&\alpha&=%1.3f\cdot 10^{%i} " % (float(e), int(m))
        to_show += r"\end{eqnarray*}"
        plt.annotate(to_show, xy=(0.075, 0.875),
                     xycoords='axes fraction', size=14)
        plt.subplot(132)
        plt.plot(dists, widths_right, "ko")
        plt.plot([dmin, dmax], [dmin*slope_right+intercept_right, dmax*slope_right+intercept_right], linestyle="solid")
        plt.xlim(dmin_all, dmax_all)
        plt.ylim(0, wmax)
        plt.xlabel("Distance")
        plt.ylabel("Width outer")
        e, m = np.format_float_scientific(slope_right).split("e")
        to_show = r"\begin{eqnarray*} "
        to_show += r"&\kappa&=%1.3f\\" % pearson_right
        to_show += r"&\alpha&=%1.3f\cdot 10^{%i} " % (float(e), int(m))
        to_show += r"\end{eqnarray*}"
        plt.annotate(to_show, xy=(0.075, 0.875),
                     xycoords='axes fraction', size=14)

        plt.subplot(133)
        plt.plot(dists, widths_mean, "ko")
        plt.plot([dmin, dmax], [dmin*slope_mean+intercept_mean, dmax*slope_mean+intercept_mean], linestyle="solid")
        plt.xlim(dmin_all, dmax_all)
        plt.ylim(0, wmax)
        plt.xlabel("Distance")
        plt.ylabel("Width mean")
        e, m = np.format_float_scientific(slope_mean).split("e")
        to_show = r"\begin{eqnarray*} "
        to_show += r"&\kappa&=%1.3f\\" % pearson_mean
        to_show += r"&\alpha&=%1.3f\cdot 10^{%i} " % (float(e), int(m))
        to_show += r"\end{eqnarray*}"
        plt.annotate(to_show, xy=(0.075, 0.875),
                     xycoords='axes fraction', size=14)

        plt.tight_layout()
        plt.savefig(path.join(data, "arms_widths_%s.png" % arm_color))
        plt.clf()
        plt.close()

    # Find pitch angle values and variations by fitting a line into log-polar
    # representation of a spiral arms (and their parts)
    for arm_file in files_with_arm_params:
        arm_color = path.splitext(path.basename(arm_file))[0].split("_")[-1]
        # Load data
        dists, x_arms, y_arms = np.genfromtxt(arm_file, unpack=True, usecols=[0, 2, 3])
        winding = determine_winding_direction(x_arms, y_arms, xcen, ycen)
        r_arms, phi_arms, _ = cartesian_to_polar_spiral(x_arms, y_arms, xcen, ycen, winding, allow_non_monotony=True)
        u_arms = np.log(r_arms)
        # Whole arm pitch angle
        odr_fit_result = fit_by_line(phi_arms, u_arms)
        slope, intercept = odr_fit_result.beta
        slope_err = odr_fit_result.sd_beta[0]
        pitch_mean = math.degrees(math.atan(abs(slope)))
        pitch_mean_std = math.degrees(slope_err / (1+slope**2))
        fout = open(path.join(data, "pitch_values_%s.dat" % arm_color), "w")
        fout.write("pitch_mean = %1.2f +/- %1.2f\n" % (pitch_mean, pitch_mean_std))
        fout.close()

        plt.figure(figsize=(5, 5))
        plt.plot(phi_arms, u_arms, "ko")
        plt.plot(phi_arms, phi_arms*slope+intercept)
        plt.xlabel(r"$\phi$")
        plt.ylabel(r"$\log(r)$")
        if slope > 0:
            plt.annotate(r"$\mu$=%1.1f" % pitch_mean, xy=(0.075, 0.9), xycoords='axes fraction', size=16)
        else:
            plt.annotate(r"$\mu$=%1.1f" % pitch_mean, xy=(0.8, 0.9), xycoords='axes fraction', size=16)
        plt.savefig(path.join(data, "arm_fit_%s.png" % arm_color))
        plt.clf()

        # Find a variation of a pitch angle using a spatial window
        dmin, dmax = min(r_arms), max(r_arms)
        window_width = (dmax-dmin) / 3.0
        window_half_width = window_width / 2
        window_starting_position = dmin + window_half_width
        window_ending_position = dmax - window_half_width

        pitch_var_file_name = path.join(data, "pitch_variation_%s.dat" % arm_color)
        fout = open(pitch_var_file_name, "w")
        for window_position in np.linspace(window_starting_position, window_ending_position, 50):
            window_lower_bound = window_position - window_half_width
            window_upper_bound = window_position + window_half_width
            idx_inside_window = np.where((r_arms > window_lower_bound) * (r_arms < window_upper_bound))
            if len(idx_inside_window[0]) > 1:
                odr_fit_result = fit_by_line(phi_arms[idx_inside_window], u_arms[idx_inside_window])
                slope = odr_fit_result.beta[0]
                slope_err = odr_fit_result.sd_beta[0]
                pitch_window = math.degrees(math.atan(abs(slope)))
                pitch_window_std = math.degrees(slope_err / (1+slope**2))
                if abs(pitch_window_std) > abs(pitch_window):
                    continue
                fout.write("%1.3f %1.2f  %1.2f\n" % (pc_per_pix * window_position, pitch_window, pitch_window_std))
        fout.close()

        # Plot pitch variation
        if len(open(pitch_var_file_name).readlines()) > 2:
            # Fit pitch variation with a line
            x, y, yerr = np.genfromtxt(pitch_var_file_name, usecols=[0, 1, 2], unpack=True)
            odr_fit_result = fit_by_line(x, y, yerr)
            slope = odr_fit_result.beta[0]
            slope_err = odr_fit_result.sd_beta[0]
            intercept = odr_fit_result.beta[1]

            plt.errorbar(x, y, yerr)
            plt.plot([min(x), max(x)], [slope*min(x)+intercept, slope*max(x)+intercept], color="k", linestyle="--")
            plt.xlabel("D [pc]")
            plt.ylabel(r"$\mu$ [deg]")
            plt.savefig(path.join(data, "pitch_variations_%s.png" % arm_color))
            plt.clf()
            plt.close()
            fout = open(path.join(data, f"pitch_variation_fit_{arm_color}.dat"), "w")
            fout.write("slope = %1.3f +/- %1.3f [deg/kpc]" % (slope*1000, slope_err*1000))
            fout.close()

    # Fit spiral radial flux
    for arm_file in files_with_arm_params:
        arm_color = path.splitext(path.basename(arm_file))[0].split("_")[-1]
        # Load data
        dists_pc, fluxes = np.genfromtxt(arm_file, unpack=True, usecols=[1, 8])
        good_idx = np.where(fluxes > 0)
        dists_pc = dists_pc[good_idx]
        fluxes = fluxes[good_idx]
        surf_brightnesses = -2.5 * np.log10(fluxes)
        expmag_fit_result = fit_by_expmag(dists_pc, surf_brightnesses)
        plt.figure(figsize=(10, 5))
        plt.subplot(111)
        plt.plot(dists_pc, surf_brightnesses, "ko")
        plt.plot(dists_pc, expmag(expmag_fit_result.beta, dists_pc))
        plt.gca().invert_yaxis()
        to_show = r"$h=%1.2f \pm %1.2f$ kpc" % (expmag_fit_result.beta[1]/1000,
                                                expmag_fit_result.sd_beta[1]/1000)
        plt.annotate(to_show, xy=(0.6, 0.9),
                     xycoords='axes fraction', size=14)
        plt.xlabel("Dist [pc]")
        plt.ylabel(r"$\mu$ [relative units]")
        # plt.subplot(122)
        # plt.plot(dists_pc, surf_brightnesses, "ko")
        # plt.plot(dists_pc, sersic(sersic_fit_result.beta, dists_pc))
        # plt.gca().invert_yaxis()
        # to_show = "$r_e=%1.2f$ kpc\n" % (sersic_fit_result.beta[1]/1000)
        # to_show += "$n=%1.1f$" % (sersic_fit_result.beta[2])
        # plt.annotate(to_show, xy=(0.6, 0.875),
        #              xycoords='axes fraction', size=14)
        plt.savefig(path.join(data, "surf_bri_%s.png" % arm_color))
        plt.close()
        fout = open(path.join(data, "surf_bri_fit_%s.dat" % arm_color), 'w')
        fout.truncate(0)
        fout.write("# exp_scale[kpc]      +/-\n")
        fout.write("%1.3f  %1.3f" % (expmag_fit_result.beta[1]/1000, expmag_fit_result.sd_beta[1]/1000))
        fout.close()

    # Analyse the asymmetry
    for arm_file in files_with_arm_params:
        arm_color = path.splitext(path.basename(arm_file))[0].split("_")[-1]
        # Load data
        dists, widths_left, widths_right, good_fit = np.genfromtxt(arm_file, unpack=True, usecols=[1, 5, 7, 9])
        good_idx = np.where(good_fit == 1)
        if len(good_idx[0]) <= 1:
            print("Not enough points to fit lithe throught asymmetry data.")
            continue
        dists = dists[good_idx]
        widths_left = widths_left[good_idx]
        widths_right = widths_right[good_idx]
        asymmetry = (widths_right - widths_left) / widths_right
        asymmetry_clipped, *_ = stats.sigmaclip(asymmetry)
        asymmetry_median = np.median(asymmetry_clipped)
        asymmetry_std = np.std(asymmetry_clipped) / (len(asymmetry_clipped)-1)**0.5
        asymmetry_pearson = stats.pearsonr(dists, asymmetry)
        asymmetry_fit_result = fit_by_line(dists, asymmetry)
        p_value = stats.ttest_1samp(asymmetry, popmean=0.0).pvalue
        plt.figure(figsize=(10, 5))
        plt.plot(dists, asymmetry, "ko")
        plt.plot(dists, linear(asymmetry_fit_result.beta, dists))
        plt.ylim(min(asymmetry_clipped), max(asymmetry_clipped))
        plt.savefig(path.join(data, "asymmetry_%s.png" % arm_color))
        plt.clf()
        plt.close()
        fout = open(path.join(data, "asymmetry_stats_%s.dat" % arm_color), "w")
        fout.write("asymmetry  %1.3f  +/-  %1.3f  # (right-left)/right \n" % (asymmetry_median, asymmetry_std))
        fout.write("pearson %1.3f\n" % asymmetry_pearson[0])
        fout.write("slope  %1.3e  +/-  %1.3e\n" % (asymmetry_fit_result.beta[0], asymmetry_fit_result.sd_beta[0]))
        fout.write("pvalue %1.3f\n" % p_value)
        fout.close()
